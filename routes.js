//Nodejs simple server
//require http method and save it in variable called http.
//use createServer() method from http to create our server.
//add an anonymous function in create server able to receuve our res and req

const http = require("http");

http.createServer(function(req, res) {

	res.writeHead(200, {'Content-Type': 'text/plain'});
	res.end("My 2nd server for Node.js");
}).listen(8000);

console.log("Server running in http://localhost: 8000");
/* Client-Server Architecture is a computing model wherein a server hosts,
delivers and manages resources that */


/*	What is a Client?
	A client is an application which creates requests for resources from a server. A client 
	will trigger an action, in a web development context, through a URL and wait for the 
	response of the server.
*/

/*What is a Server?
	A server is able to host and deliver resources requested by a client. In fact, a single
	server can handle multiple clients.
*/

/*What is Nodejs?
	Node.js is an environment to be able to develop applications with Javascript. With this,
	we can run JS even without the use of an HTML.
	
	Javascript was originally conceptualized to be used for frontend applications. This is the reason 
	why our vanilla javascript had to be linked and connected to an HTML file before we can run the JS. 

	But with the advent of Node.js, JS can now be used to create backend applications/servers.

		Note: 
		*Front-End/Client is usually the page that we're seeing. It is the client application the request 
		resources from a server or a backend. 

		*Back-end/Server applications are usually server-related applications which handles requests from a 
		Front-end/client.
	Why is Node.js so popular? 
		Performance - Nodejs is one of the most performing environment for creating backend applciation for JS
		
		Familiarity - It uses JS as its language and therefore very familiar for most developer.

		NPM - Node Package Manager - is tha largest registry for node packages. Theses packages are small bits
		of programs, methods, functions and codes that greatly help in the development of an application.

*/

/* We are now able to run a simple nodejs server. Wherein when we added our URL in the browser, the browser
client actually requested to our server and our server was able to respond with a text.


   We used require() method to load node.js modules.

   		A module is a software component or part of a program which contains one or more routines.

   		The http module is a default module from node.js

   		The http module let nodejs transfer data or let our client and server exchange data via Hypertext
   		Transfer Protocol

   		protocol => http://localhost: 4000* <= server 

		With this, the client, which is our browser, automatically created a request and our server
		was able to respond with a message.

		This is how clients and serves communicate with each other. A client triggers an action from as 
		server via URL and the server responds with the data.
	
		Note: 
		Messages from a client which triggers an action is called a request.
	
		Messages from a server to respond to a client's request is called response. 

		.createServer() method has an anonymous function that handles our clients and our server response.
		The anonymous function in the createServer method is able to receive two objects, first, req or 
		request, this is the request form the client. second, is res, this our server's response.
		Each request and response parameters are object which contains the details of a request or response
		as well as methods to handle them.


		res.end() is a method of the response object which ends the server's response and sends 
		a message/data as a string.

		.listen() allows us to assign a port to a server. There are several tasks and 
		process in our computer which are also designated into their specific ports.
   		*/


let http = require("http");
//console.log(http);
http.createServer(function(req, res) {
	/* We can actually respond differently to different requests

   When our browser requests through server the complete URL is like this:

   http://localhost: 4000/ - is called an endpoint.

   We can respond differently to different request URL endpoints.
*/

//.url is a property of the request object pertaining to the endpoint of the URL.
	
	if(req.url === "/") {
	res.writeHead(200, {'Content-Type': 'text/plain'});
	res.end('Hi my name is Blue');
	console.log(req.url);
	} 
	else if (req.url === "/login") {
	res.writeHead(200, {'Content-Type': 'text/plain'});
	res.end('Welcome to the login page.');
	console.log(req.url);
	}
	else { 
		//how can we respond to request with url endpoints that we don't have a route for? 
		//this will be our response if an endpoint passed in the client's request URL is not
		//recognized or there is no designated route for that endpoint.
		res.writeHead(404, {'Content-Type': 'text/plain'});
		res.end('Resource can not be found.');
		console.log(req.url);
	}
	



}).listen(4000);
console.log('Server is running on localHost: 4000');